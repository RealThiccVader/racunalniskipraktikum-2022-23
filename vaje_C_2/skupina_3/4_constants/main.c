
#include <stdio.h>

int multiply_by_constant(int number);

const int MY_CONSTANT = 100;

int main() {
    int input = 0;
    printf("Enter a number: ");
    scanf("%i", &input);

    int multiplied = multiply_by_constant(input);
    printf("Mult: %i\n", multiplied);
}

int multiply_by_constant(int number) {
    return number * MY_CONSTANT;
}