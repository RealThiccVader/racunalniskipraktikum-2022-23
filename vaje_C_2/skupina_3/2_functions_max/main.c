#include <stdio.h>
/*
    DEKLARACIJA FUNKCIJ
*/

int max(int num1, int num2);

/*
    INICIALIZACIJA FINKCIJ
*/
int main() {
    int number_one = 0; // int number_one; != int number_one = 0;
    printf("Enter first number: ");
    scanf("%i", &number_one);

    int number_two = 0;
    printf("Enter second number: ");
    scanf("%i", &number_two);

    int greater = max(number_one, number_two);
    printf("Greater: %i\n", greater);

}

int max(int num1, int num2) {
    if (num1 > num2) {
        return num1;
    } else {
        return num2;
    }
}