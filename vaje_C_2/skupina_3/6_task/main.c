/**
    NAL: napiši program, ki uporabniku pretvori km/h v m/s in obratno.
    Uporabnik smer pretvorbe izbere z vnosom št. 1 ali 2.
    Implementiraj funckiji (to_kmh in to_mps) za pretvorbo ter za 
    pretvorbo uporabljaj konstante.
    1 m/s = 3.6 km/h
    
    IZPIS:
    Izberi smer pretvorbe:
    1) kmh -> mps
    2) mps -> kmh
    2
    Vpiši količino: 1
    Kmh: 3.600000
*/
#include <stdio.h>

double to_mps(double ammount);
double to_kmh(double ammount);

const double COEFICIENT = 3.6;

int main() {
    printf("Izberi smer pretvorbe:\n\t1) kmh -> mps\n\t2) mps -> kmh\n");

    int direction = 0;
    scanf("%i", &direction);

    double ammount = 0.0;
    printf("Vpisi kolicino: ");
    scanf("%lf", &ammount);

    if (direction == 1) {
        double mps = to_mps(ammount);
        printf("Mps: %lf\n", mps);
    } else {
        double kmh = to_kmh(ammount);
        printf("Kmh: %lf\n", kmh);
    }

}

double to_mps(double ammount) {
    return ammount / COEFICIENT;
}

double to_kmh(double ammount) {
    return ammount * COEFICIENT;
}