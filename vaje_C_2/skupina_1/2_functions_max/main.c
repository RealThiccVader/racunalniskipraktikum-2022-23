/**
    NAL: napiši program, ki od uporabnika sprejme 2 števili.
    Implementiraj funckijo "max", ki ji podamo te števili, ta pa nam vrne večjo.
    Večje število izpiši.

    IZPIS:
    Vpisi stevilo a: 5
    Vpisi stevilo b: 8
    Vecje stevilo je: 8
*/
#include <stdio.h>
/*
    DEKLARACIJA FUNKCIJ
*/
int max(int st1, int st2);
/*
    INICIALIZACIJA FUNKCIJ
*/

int main() {
    int prvo_stevilo;
    int drugo_stevilo;
    printf("Vpisi prvo stevilo: ");
    scanf("%i", &prvo_stevilo);

    printf("Vpisi drugo stevilo: ");
    scanf("%i", &drugo_stevilo);

    int vecje = max(prvo_stevilo, drugo_stevilo);
    printf("Vecje stevilo: %i\n", vecje);

}

int max(int st1, int st2) {
    if (st1 > st2) {
        return st1;
    } else {
        return st2;
    }
}