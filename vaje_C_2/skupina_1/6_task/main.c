/**
    NAL: napiši program, ki uporabniku pretvori km/h v m/s in obratno.
    Uporabnik smer pretvorbe izbere z vnosom št. 1 ali 2.
    Implementiraj funckiji (to_kmh in to_mps) za pretvorbo ter za 
    pretvorbo uporabljaj konstante.
    1 m/s = 3.6 km/h
    
    IZPIS:
    Izberi smer pretvorbe:
    1) kmh -> mps
    2) mps -> kmh
    2
    Vpiši količino: 1
    Kmh: 3.600000
*/

#include <stdio.h>

double to_kmh(double val);
double to_mps(double val);

const double COEFICIENT = 3.6;

int main() {
    printf("Izberi smer pretvorbe:\n\t1) kmh -> mps\n\t2) mps -> kmh\n");
    
    int direction;
    double ammount;

    scanf("%i", &direction);
    
    printf("Vpisi kolicion: ");
    scanf("%lf", &ammount);

    if (direction == 1) {
        double converted = to_mps(ammount);
        printf("Mps: %lf\n", converted);
    } else {
        double converted = to_kmh(ammount);
        printf("kmh: %lf\n", converted);
    }
}

double to_kmh(double val) {
    return val * COEFICIENT;
}

double to_mps(double val) {
    return val / COEFICIENT;
}