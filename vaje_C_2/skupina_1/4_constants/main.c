/**
    NAL: napiši program, ki od uporabnika sprejme 1 število.
    Implementiraj funckijo "multiply_by_constant", ki ji podamo to število, ta 
    pa nam število pomnoži z konstanto.
    Večje število izpiši.

    IZPIS: 
    Vpisi stevilo: 5
    Rezultat: 500
*/
#include <stdio.h>
/*
    DEKLARACIJA FUNKCIJ
*/
int multiply_by_constant(int stevilo);
/*
    INICIALIZACIJA GLOBALNIH KONSTANT
*/
const int CONSTANT = 100;
/*
    INICIALIZACIJA FUNKCIJ
*/
int main() {
    int input;
    printf("Enter number: ");
    scanf("%i", &input);

    int multiplied_input = multiply_by_constant(input);
    printf("Multiplied input: %i\n", multiplied_input);
}

int multiply_by_constant(int stevilo) {
    return stevilo * CONSTANT;
}