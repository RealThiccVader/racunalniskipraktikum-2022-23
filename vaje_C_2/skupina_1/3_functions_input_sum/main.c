#include <stdio.h>
/*
    DEKLARACIJA FUNKCIJ
*/
int get_input_from_user();
/*
    INICIALIZACIJA FUNKCIJ
*/

int main() {
    int number_of_requests = 3;
    int sum = 0;

    for (int i = 0 ; i < number_of_requests ; i++) {
        sum = sum + get_input_from_user();
    }

    printf("Sum is: %i\n", sum);
}

int get_input_from_user() {
    int input;
    printf("Enter a number: ");
    scanf("%i", &input);
    return input;
}