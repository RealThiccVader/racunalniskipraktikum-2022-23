/**
    NAL: napiši program, ki od uporabnika sprejme 1 število.
    Implementiraj funckijo "multiply_by_constant", ki ji podamo to število, ta 
    pa nam število pomnoži z konstanto.
    Večje število izpiši.

    IZPIS: 
    Vpisi stevilo: 5
    Rezultat: 500
*/

#include <stdio.h>
/*
    deklaracija funkcij
*/
int multiply_by_constant(int num);

/*
    inicializacija globalnih konstant
*/ 
const int CONSTANT = 100;
/*
    inicializacija funkcij
*/ 
int main() {
    int input;

    printf("Vpisi stevilo ");
    scanf("%i", &input);
    int pomnozen_vnos = multiply_by_constant(input);
    printf("Rezltat: %i\n", pomnozen_vnos);
}

int multiply_by_constant(int num) {
    int zmnozek = num * constant;
    return zmnozek;
}