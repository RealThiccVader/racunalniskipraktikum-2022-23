#include <stdio.h>
/*
    deklaracije funkcij
*/
int max(int num1, int num2);
/*
    inicializacije funkcij
*/
int main() {
    int prvo;
    printf("Vpisi prvo stevilo: ");
    scanf("%i", &prvo);

    int drugo;
    printf("Vpisi drugo stevilo: ");
    scanf("%i", &drugo);

    int vecja = max(prvo, drugo);
    printf("Vecja: %i\n", vecja);
}

int max(int num1, int num2) {
    if (num1 > num2) {
        return num1;
    } else {
        return num2;
    }
}