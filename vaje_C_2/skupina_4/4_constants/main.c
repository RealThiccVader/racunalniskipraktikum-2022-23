#include <stdio.h>
/*
    deklaracije funkcij
*/
int multiply_by_constant(int number);
int get_user_input();
/*
    globalne konstante
*/
const int MY_CONSTANT = 100;
/*
    inicializacije funkcij
*/
int main() {
    int input = get_user_input();
    int stevilo = multiply_by_constant(input);
    printf("Stevilo: %i\n", stevilo);
}

int multiply_by_constant(int number) {
    return number * MY_CONSTANT;
}

int get_user_input() {
    int input;
    printf("Vpisi stevilo: ");
    scanf("%i", &input);
    return input;
}