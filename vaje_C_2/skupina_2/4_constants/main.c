#include <stdio.h>
/*
    function decl.
*/
int get_user_input();
int multiply_by_constant(int number);

/*
    GLOBAL CONSTATNS
*/
const int CONSTANT_NAME = 100;

/*
    func init
*/
int main() {
    int input = get_user_input();
    int multiplied = multiply_by_constant(input);
    printf("Multiplied: %i\n", multiplied);
}

int multiply_by_constant(int number) {
    return number * CONSTANT_NAME;
}

int get_user_input() {
    int input;
    printf("Enter number: ");
    scanf("%i", &input);
    return input;
}