/**
    NAL: napiši program, ki uporabniku pretvori km/h v m/s in obratno.
    Uporabnik smer pretvorbe izbere z vnosom št. 1 ali 2.
    Implementiraj funckiji (to_kmh in to_mps) za pretvorbo ter za 
    pretvorbo uporabljaj konstante.
    1 m/s = 3.6 km/h
    
    IZPIS:
    Izberi smer pretvorbe:
    1) kmh -> mps
    2) mps -> kmh
    2
    Vpiši količino: 1
    Kmh: 3.600000
*/
#include <stdio.h>

double to_kmh(double num);
double to_mps(double num);

const double COEFICIENT = 3.6;

int main() {
    printf("Izberi smer pretvorbe:\n1) kmh -> mps\n2) mps -> kmh\n");

    int direction;
    double ammount;

    scanf("%i", &direction);
    printf("Ammount: ");
    scanf("%lf", &ammount);

    if (direction == 1) {
        double mps = to_mps(ammount);
        printf("Mps: %fl\n", mps);
    } else {
        double kmh = to_kmh(ammount);
        printf("Kmh: %fl\n", kmh);
    }
}

double to_kmh(double num) {
    return num * COEFICIENT;
}
double to_mps(double num) {
    return num / COEFICIENT;
}