#include <stdio.h>
/*
    function declaration
*/
int get_user_input();
/*
    function initialization
*/
int main() {
    int number_of_requests = 5;
    int sum = 0;

    for (int i = 0 ; i < number_of_requests ; i++) {
        int input = get_user_input();
        sum = sum + input;
    }

    printf("User input: %i\n", sum);
}

int get_user_input() {
    int input;
    printf("Enter number: ");
    scanf("%i", &input);
    return input;
}