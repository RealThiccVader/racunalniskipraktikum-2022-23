#include <stdio.h>

/*
    DEKLARACIJE FUNKCIJ
*/
int max(int num1, int num2);

/*
    INICIALIZACIJE FINKCIJ
*/
int main() {
    int first_number;
    printf("Enter first number: ");
    scanf("%i", &first_number);

    int second_number;
    printf("Enter second number: ");
    scanf("%i", &second_number);

    int bigger = max(first_number, second_number);
    printf("Bigger: %i\n", bigger);
}

int max(int num1, int num2) {
    if (num1 > num2) {
        return num1;
    } else {
        return num2;
    }
}