#include <stdio.h>

/*
    DEKLARACIJE FUNKCIJ
*/
int max(int num1, int num2);

/*
    INICIALIZACIJE FUNKCIJ
*/
int main() {
    int num1;
    printf("Enter a number: ");
    scanf("%i", &num1);

    int num2;
    printf("Enter a number: ");
    scanf("%i", &num2);

    int bigger = max(num1, num2);
    printf("Bigger: %i\n", bigger);   
}

int max(int num1, int num2) {
    if (num1 > num2) {
        return num1;
    } else {
        return num2;
    }
}