#include <stdio.h>
/*
    dekleracije funkcij
*/
int get_user_input();
int multiply_by_constant(int number);
/*
    globalne konstante
*/
const int MY_CONSTANT = 100;
/*
    inicializacije funkcij
*/
int main() {
    int input = get_user_input();
    int mult = multiply_by_constant(input);
    printf("Mult: %i\n", mult);
}

int multiply_by_constant(int number) {
    int mult = number * MY_CONSTANT;
    return mult;
}

int get_user_input() {
    int input;
    printf("Vnesi stevilo: ");
    scanf("%i", &input);
    return input;
}