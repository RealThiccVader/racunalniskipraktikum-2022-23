#include <stdio.h>

int main() {
    int numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    int size_of_numbers = sizeof(numbers);
    printf("Size of numbers: %i\n", size_of_numbers);

    int size_of_element = sizeof(numbers[0]);
    printf("Size of element: %i\n", size_of_element);

    int length_of_numbers = size_of_numbers / size_of_element;
    printf("Length of numbers: %i\n", length_of_numbers);

    
    for (int i = 0 ; i < length_of_numbers ; i++) {
        printf("Number on index %i: %i\n", i, numbers[i]);
    }
}