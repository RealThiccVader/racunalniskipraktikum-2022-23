#include <stdio.h>

int main() {
    int number = 5;
    // int[] numbers = new int[5];
    int numbers[5];
    numbers[0] = 10;
    numbers[1] = 5;
    numbers[2] = 20;
    numbers[3] = 15;
    numbers[4] = 8;

    for (int i = 0 ; i < 5 ; i++) {
        printf("Number on index %i: %i\n", i, numbers[i]);
    }

    printf("New array!\n");

    int other_numbers[] = { 1, 2, 3, 4, 5 };

    for (int i = 0 ; i < 5 ; i++) {
        printf("Number on index %i: %i\n", i, other_numbers[i]);
    }
}