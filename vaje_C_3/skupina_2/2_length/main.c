#include <stdio.h>

int array_length(int numbers[]);

int main() {
    int numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    //array_length(numbers); <--- NO WORK!

    
    int size_of_numbers = sizeof(numbers);
    printf("Size of array numbers: %i\n", size_of_numbers);

    int size_of_element = sizeof(numbers[0]);
    printf("Size of array element: %i\n", size_of_element);

    int length_of_numbers = size_of_numbers / size_of_element;
    printf("Length of array numbers: %i\n", length_of_numbers);

    for (int i = 0 ; i < length_of_numbers ; i++) {
        printf("Stevilo na mestu %i: %i\n", i, numbers[i]);
    }
}

int array_length(int numbers[]) {
    int size_of_numbers = sizeof(numbers);
    printf("Size of array numbers: %i\n", size_of_numbers);

    int size_of_element = sizeof(numbers[0]);
    printf("Size of array element: %i\n", size_of_element);

    int length_of_numbers = size_of_numbers / size_of_element;
    printf("Length of array numbers: %i\n", length_of_numbers);
    return length_of_numbers;
}