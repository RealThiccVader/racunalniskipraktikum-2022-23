#include <stdio.h>

int main() {
    printf("Creating first array...");
    // int[] numbers = new int[5];
    int numbers[5];
    numbers[0] = 5;
    numbers[1] = 10;
    numbers[2] = 8;
    numbers[3] = 1;
    numbers[4] = 5;
    printf("Done!\n");

    for (int i = 0 ; i < 5 ; i++) {
        printf("Stevilo na mestu %i: %i\n", i, numbers[i]);
    }

    printf("Crating second array...");
    int other_numbers[5] = { 1, 2, 3, 5, 5 };
    printf("Done!\n");
    for (int i = 0 ; i < 5 ; i++) {
        printf("Stevilo na mestu %i: %i\n", i, other_numbers[i]);
    }
}