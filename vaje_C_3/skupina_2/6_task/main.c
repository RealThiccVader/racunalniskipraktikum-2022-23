#include <stdio.h>
#include <stdlib.h>

void print_array(int numbers[], int len);
void sort(int numbers[], int len);

int main() {
    int array_len = 10;
    int numbers[array_len];

    for (int i = 0 ; i < array_len ; i++) {
        numbers[i] = rand() % 1000;
    }

    print_array(numbers, array_len);
    sort(numbers, array_len);
    print_array(numbers, array_len);
}

void print_array(int numbers[], int len) {
    printf("[ ");
    for (int i = 0 ; i < len ; i++) {
        printf("%i ", numbers[i]);
    }
    printf("]\n");
}

void sort(int numbers[], int len) {
    for (int i = 0 ; i < len - 1 ; i++) {
        for (int j = 0 ; j < len - 1 ; j++) {
            if (numbers[j] > numbers[j + 1]) {
                int tmp = numbers[j + 1];
                numbers[j + 1] = numbers[j];
                numbers[j] = tmp;
            }
        }
    }
}

