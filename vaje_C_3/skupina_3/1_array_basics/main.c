#include <stdio.h>

int main() {
    printf("Creating first array...");
    // int[] numbers = new int[5];
    int numbers[5];
    numbers[0] = 3;
    numbers[1] = 5;
    numbers[2] = 10;
    numbers[3] = 1;
    numbers[4] = 2;
    printf("Done!\n");

    for (int i = 0 ; i < 5 ; i++) {
        printf("Element na indexu %i: %i\n", i, numbers[i]);
    }

    printf("Creating second array...");
    int other_numbers[5] = { 1, 2, 3, 4, 5 };
    printf("Done!\n");
    for (int i = 0 ; i < 5 ; i++) {
        printf("Element na indexu %i: %i\n", i, other_numbers[i]);
    }
}