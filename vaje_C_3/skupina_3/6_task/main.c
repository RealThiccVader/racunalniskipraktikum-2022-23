#include <stdio.h>
#include <stdlib.h>

void print_array(int numbers[], int len);
void sort(int numbers[], int len);

int main() {
    srand(200);
    int length_of_numbers = 10;
    int numbers[length_of_numbers];
    for (int i = 0 ; i < length_of_numbers ; i++) {
        // fill the numbers array
        numbers[i] = rand() % 1000;
    }
    print_array(numbers, length_of_numbers);
    sort(numbers, length_of_numbers);
    print_array(numbers, length_of_numbers);
}

void sort(int numbers[], int len) {
    for (int i = 0 ; i < len - 1 ; i++) {
        for (int j = 0 ; j < len - 1 - i ; j++) {
            if (numbers[j] > numbers[j + 1]) {
                int tmp = numbers[j + 1];
                numbers[j + 1] = numbers[j];
                numbers[j] = tmp;
            }
        }
    }
} 

void print_array(int numbers[], int len) {
    printf("[ ");
    for (int i = 0 ; i < len ; i++) {
        printf("%i ", numbers[i]);
    }
    printf("]\n");
}