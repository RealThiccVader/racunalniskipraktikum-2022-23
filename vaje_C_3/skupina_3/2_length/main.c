#include <stdio.h>

int main() {
    int numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int size_of_numbers = sizeof(numbers);
    printf("Size of numbers: %i\n", size_of_numbers);

    int size_of_element = sizeof(numbers[0]);
    printf("Size of one element: %i\n", size_of_element);

    int numbers_length = size_of_numbers / size_of_element;
    printf("Length of array numbers: %i\n", numbers_length);
      
    for (int i = 0 ; i < numbers_length ; i++) {
        printf("%i ", numbers[i]);
    }
    printf("\n");

}
