
/*
    NAL: izpiši tri točke. Točke shrani z uporabo večdimenzionalnih polj. 
    Za izpis napiši funkcijo "print_points", ki sprejme polje točk.

    IZPIS:
    Tocka 1: (1, 1)
    Tocka 2: (3, 5)
    Tocka 3: (3, 2)
*/

#include <stdio.h>

void print_points(int points[][2], int length);

int main() {
    int points[3][2] = {{1, 1}, {3, 5}, {3, 2}};
    int points_length = sizeof(points) / sizeof(points[0]);
    printf("Length of points: %i\n", points_length);

    print_points(points, points_length);
}

void print_points(int points[][2], int length) {
    for (int i = 0 ; i <= length ; i++) {
        int x = points[i][0];
        int y = points[i][1];
        printf("Tocka %i: (%i, %i)\n", i + 1, x, y);
    }
}