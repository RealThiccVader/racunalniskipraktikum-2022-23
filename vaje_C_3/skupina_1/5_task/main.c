/**
    NAL: Od uporabnika sprejmi 5 števil in jih shrani v polje. Ko uporabnik vpiše vseh 5 števil,
    naj program izpiše polje uporabniku v nasprotnem vrstnem redu. V programu implementiraj funkcijo 
    "void print_reverse(int numbers[], int len)", ki prejme polje števil in njegovo dolžino, ter ne 
    vrača ničesar, temveč izpiše števila kot prikazano spodaj.

    IZPIS: 
    Vpiši število: 6
    Vpiši število: 1
    Vpiši število: 3
    Vpiši število: 20
    Vpiši število: 8
    [ 8 20 3 1 6 ]
*/
#include <stdio.h>

int get_user_input();
void print_reverse(int numbers[], int len);

int main() {
    int number_of_requests = 5;
    int numbers[number_of_requests];
    for (int i = 0 ; i < number_of_requests ; i++) {
        numbers[i] = get_user_input();
    }
    print_reverse(numbers, number_of_requests);
}

void print_reverse(int numbers[], int len) {
    printf("[ ");
    for (int i = len - 1 ; i >= 0 ; i--) {
        printf("%i ", numbers[i]);
    }
    printf("]\n");
}

int get_user_input() {
    int input;
    printf("Vpisi stevilo: ");
    scanf("%i", &input);
    return input;
}