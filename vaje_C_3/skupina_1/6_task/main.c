#include <stdio.h>
#include <stdlib.h>

void sort(int numbers[], int len);
void print_array(int numbers[], int len);

int main() {
    int ammount_of_numbers = 10;
    int numbers[ammount_of_numbers];

    for (int i = 0 ; i < ammount_of_numbers ; i++) {
        numbers[i] = rand() % 100;
    }

    print_array(numbers, ammount_of_numbers);
    sort(numbers, ammount_of_numbers);
    print_array(numbers, ammount_of_numbers);
}

void sort(int numbers[], int len) {
    for (int i = 0 ; i < len - 1 ; i++) {
        for (int j = 0 ; j < len - 1 - i ; j++) {
            if (numbers[j] > numbers[j + 1]) {
                int tmp = numbers[j + 1];
                numbers[j + 1] = numbers[j];
                numbers[j] = tmp;
            }
        }
    }
}

void print_array(int numbers[], int len) {
    printf("[ ");
    for (int i = 0 ; i < len ; i++) {
        printf("%i ", numbers[i]);
    }
    printf("]\n");
}