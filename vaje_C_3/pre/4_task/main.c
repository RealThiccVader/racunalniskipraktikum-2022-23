/**
    NAL: Od uporabnika sprejmi 5 števil in jih shrani v polje. Ko uporabnik vpiše vseh 5 števil,
    naj program poišče največje število in ga izpiše. V programu implementiraj funkcijo 
    "int max(int numbers[], int len)", ki prejme polje števil in njegovo dolžino, ter vrne največje 
    število.

    IZPIS: 
    Vpiši število: 6
    Vpiši število: 1
    Vpiši število: 3
    Vpiši število: 20
    Vpiši število: 8
    Največje število: 20
*/