#include <stdio.h>

void print_array(int array[], int length);
int array_length(int array[]);

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    int size_of_numbers = sizeof(numbers);
    printf("Size of whole array: %i\n", size_of_numbers);

    int size_of_elt = sizeof(numbers[0]);
    printf("Size of one element: %i\n", size_of_elt);

    int length_of_numbers = size_of_numbers / size_of_elt;
    printf("Length of numbers: %i\n", length_of_numbers);
    
    print_array(numbers, length_of_numbers);   
}

int array_length(int numbers[]) {
    /*
        does not work as expected!
        We'll see why next class
    */
    int size_of_numbers = sizeof(numbers);
    printf("Size of whole array: %i\n", size_of_numbers);

    int size_of_elt = sizeof(numbers[0]);
    printf("Size of one element: %i\n", size_of_elt);

    int length_of_numbers = size_of_numbers / size_of_elt;
    printf("Length of numbers: %i\n", length_of_numbers);
    
    return length_of_numbers;
}

void print_array(int array[], int len) {
    printf("[ ");
    for (int i = 0 ; i < len ; i++) {
        printf("%i ", array[i]);
    }
    printf("]\n");
}
