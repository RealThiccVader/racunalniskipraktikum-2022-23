#include <stdio.h>

int main() {

    // int[] numbers = new int[5]
    int numbers[5];
    numbers[0] = 3;
    numbers[1] = 5;
    numbers[2] = 8;
    numbers[3] = 1;
    numbers[4] = 9;

    for (int i = 0 ; i < 5 ; i++) {
        printf("Stevilo na indexu %i: %i\n", i, numbers[i]);
    }
    printf("New array\n");
    int other_numbers[5] = { 1, 2, 3, 4, 5 };

    for (int i = 0 ; i < 5 ; i++) {
        printf("Stevilo na indexu %i: %i\n", i, other_numbers[i]);
    }
}