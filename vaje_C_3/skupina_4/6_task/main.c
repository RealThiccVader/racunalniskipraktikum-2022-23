#include <stdio.h>
#include <stdlib.h>

void print_array(int numbers[], int len);
void sort(int numbers[], int len);

int main() {
    int numbers_length = 10;
    int numbers[numbers_length];
    for (int i = 0 ; i < numbers_length ; i++) {
        // insert random number
        numbers[i] = rand() % 1000;
    }
    print_array(numbers, numbers_length);
    sort(numbers, numbers_length);
    print_array(numbers, numbers_length);
}

void sort(int numbers[], int len) {
    for (int i = 0 ; i < len - 1 ; i++) {
        for (int j = 0 ; j < len - 1 ; j++) {
            if (numbers[j] > numbers[j + 1]) {
                int tmp = numbers[j + 1];
                numbers[j + 1] = numbers[j];
                numbers[j] = tmp;
            }
        }
    }
}

void print_array(int numbers[], int len) {
    printf("[ ");
    for (int i = 0 ; i < len ; i++) {
        printf("%i ", numbers[i]);
    }
    printf("]\n");
}