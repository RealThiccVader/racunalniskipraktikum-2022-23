/**
    NAL: Od uporabnika sprejmi 5 števil in jih shrani v polje. Ko uporabnik vpiše vseh 5 števil,
    naj program poišče največje število in ga izpiše. V programu implementiraj funkcijo 
    "int max(int numbers[], int len)", ki prejme polje števil in njegovo dolžino, ter vrne največje 
    število.

    IZPIS: 
    Vpiši število: 6
    Vpiši število: 1
    Vpiši število: 3
    Vpiši število: 20
    Vpiši število: 8
    Največje število: 20
*/
#include <stdio.h>

int max(int numbers[], int len);

int main() {
    int number_of_requests = 5;
    int numbers[number_of_requests];
    for (int i = 0 ; i < number_of_requests ; i++) {
        printf("Vpisi stevilo: ");
        scanf("%i", &numbers[i]);
    }
    int biggest_number = max(numbers, number_of_requests);
    printf("Najvecje stevilo: %i\n", biggest_number);
}

int max(int numbers[], int len) {
    int biggest = numbers[0];
    for (int i = 0 ; i < len ; i++) {
        if (numbers[i] > biggest) {
            biggest = numbers[i];
        }
    }
    return biggest;
}