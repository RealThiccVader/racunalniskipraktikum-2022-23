# STUDENT SUBMISSIONS

Any student that wants to share their repositories relating to the subject "Računalniški praktikum" that will benefit others can follow this guide on how to do so.

## Step 1 - fork the repository

* On the official repository of ["Računalniški praktikum"](https://gitlab.com/VakeFAMNIT/racunalniskipraktikum-2022-23) go to the fork button on the top right corner <kbd>
    <img src="READMEDependencies/code-fork-solid.svg" width=20px height=20px>
    Fork
</kbd>

* On this page don't change the project name, just pick a namespace from the drop-down list
https​://gitlab.com/[Your namespace].
</kbd>

* Click <kbd>
Fork project
</kbd>

## Step 2 - clone the project and add a submodule
### → Cloning
* On your machine open the folder where you want to clone the repository.

* To clone it use: 

    ```
    git clone https​://gitlab.com/[Your username]/racunalniskipraktikum-2022-23.git
    ```

### → Submodule
* Move to the student_submissions folder using.
    ```
    cd student_submissions
    ```

* To add new git submodule use:
    ```
    git submodule add [Https_link_to_your_repository]
    ```

## Step 3 - push it onto the forked repository

* Use add command to update the index using the current content found in the working tree, to prepare the content staged for the next commit.
    ```
    git add student_submissions/[Your_directory_name]
    ```

* Create a new commit containing the current contents of the index and the given log message describing the changes.
    ```
    git commit -m "Added new student submission"
    ```

* Use push to update remote refs using local refs, while sending objects necessary to complete the given refs.
    ```
    git push -u
    ```

## Step 4 - send a merge(pull) request

* On the left side of your GitLab account click <kbd>
merge request
</kbd>

* Fill out the source branch and target branch information.

* Click <kbd>
Compare branch and continue
</kbd>

* Fill out the title with your first and last name. As well as your course, year and group. Example:
    ```
    Jon Doe BF1-2
    ```

* Fill out the description with information about your repository.

* Select <kbd>
Squash commits when merge request is accepted.
</kbd>

* Click <kbd>
Create merge request
</kbd>

## Resources

* [Repository](https://docs.gitlab.com/ee/user/project/repository/#clone-a-repository)

* [Project forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

* [7.11 Git Tools - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

* [Squash and merge](https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html)

* [Creating merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

## Author
    This README file with instructions was created by Sever Trifunović(@WideVader) BF1 16.12.2022. 
    Live long and prosper!