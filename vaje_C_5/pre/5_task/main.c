/**
    NAL: Ustvarite nov program C in definirajte funkcijo z imenom "bounds". Funkcija naj
    prejme 4 argumente. Prvi argument naj bo polje števil (int array) in drugi argument 
    njegova dolžina. Tretji in četrti argument pa naj bosta kazacla na spremenjlivki tipa 
    int. V prvega od teh shrani vrednost najmanjšega elementa polja, v drugega pa največjega.
    V main funkciji definiraj polje, ki je velikost vsaj 10 elementov, ter spremenjlivki max
    in min. Pokliči funkcijo "bounds" in izpiši vrednosti min in max.

    IZPIS:
    Najmanjša vrednost: -120
    Največja vrednost: 75
*/
#include <stdio.h>

void bounds(int numbers[], int len, int* min, int* max);
void normalize(int numbers[], double normalized[], int len, int min, int max);

int main() {
    int my_numbers[10] = {1, 2, 3, -120, 5, 6, 75, 8, 9, 0};
    int min, max;
    bounds(my_numbers, 10, &min, &max);
    printf("Najmanjsa vrednost: %i\n", min);
    printf("Najvecja vrednost: %i\n", max);
    // EXTRA: normalize array (0 -> 1)
    double normalized[10];
    normalize(my_numbers, normalized, 10, min, max);
    for (int i = 0 ; i < 10 ; i++) {
        printf("%lf ", normalized[i]);
    }

}

void bounds(int numbers[], int len, int* min, int* max) {
    int min_value = numbers[0];
    int max_value = numbers[0];
    for (int i = 0 ; i < len ; i++) {
        if (numbers[i] > max_value) {
            max_value = numbers[i];
        }
        if (numbers[i] < min_value) {
            min_value = numbers[i];
        }
    }
    *min = min_value;
    *max = max_value;
}

void normalize(int numbers[], double normalized[], int len, int min, int max) {
    if (min < 0) {
        for (int i = 0 ; i < len ; i++) {
            numbers[i] += -min;
        }
        max += -min;
    }
    for (int i = 0 ; i < len ; i++) {
        normalized[i] = ((double) numbers[i]) / ((double)max );
    }
}