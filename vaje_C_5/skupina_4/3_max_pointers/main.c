#include <stdio.h>

int* max(int* a, int* b);

int main() {
    int num1 = 5;
    int num2 = 8;
    printf("[main] num1: %i\n", num1);
    printf("[main] num2: %i\n", num2);
    printf("[main] &num1: %p\n", &num1);
    printf("[main] &num2: %p\n", &num2);
    int* bigger = max(&num1, &num2);
    printf("[main] bigger: %p\n", bigger);
    printf("[main] *bigger: %i\n", *bigger);
    num2 = 3;
    printf("[main] *bigger: %i\n", *bigger);
}

int* max(int* a, int* b) {
    printf("[max] a: %p\n", a);
    printf("[max] b: %p\n", b);
    if (*a > *b) {
        return a;
    } else {
        return b;
    }
}