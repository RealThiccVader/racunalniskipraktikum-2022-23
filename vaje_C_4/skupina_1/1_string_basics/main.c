#include <stdio.h>

int main() {
    // String string = "blabla";
    char first_string[20] = "Bla bla";

    char second_string[] = {'B', 'l', 'a', ' ', 'b', 'l', 'a', '\0'}; 

    for (int i = 0 ; i < 8 ; i++) {
        printf("%c", second_string[i]);
    }
    printf("\n");

    int index = 0;
    while (second_string[index] != '\0') {
        printf("%c", second_string[index]);
        index++;
    }
    printf("\n");

    printf("%s\n", second_string);
}