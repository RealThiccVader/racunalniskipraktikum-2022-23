#include <stdio.h>
#include <stdbool.h>

void clone_string(char from[], char to[]);

int main() {
    char origin[50] = "12345";
    char destination[50];
    clone_string(origin, destination);
    printf("Origin string: %s\n", origin);
    printf("Destination string: %s\n", destination);


    bool is_end = destination[25] == '\0';
    if (is_end) {
        printf("JE!\n");
    } else {
        printf("NI?\n");
    }

}

void clone_string(char from[], char to[]) {
    int index = 0;
    while(from[index] != '\0') {
        to[index] = from[index];
        index++;
    }
    to[index] = '\0';
    printf("%i\n", index);
}