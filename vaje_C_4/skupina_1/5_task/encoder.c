#include <stdio.h>

void encode(char str[], int offset);

int main() {
    int offset = 3;
    char input[100];
    printf("Enter string to encode: ");
    fgets(input, sizeof(input), stdin);

    encode(input, offset);
    printf("Encoded message:\n%s\n", input);
}

void encode(char str[], int offset) {
    int index = 0;
    while (str[index] != '\0') {
        str[index] += offset;
        index++;
    }
}