#include <stdio.h>

void decode(char str[], int offset);

int main() {
    int offset = 3;
    char input[100];
    printf("Enter string to decode: ");
    fgets(input, sizeof(input), stdin);

    decode(input, offset);
    printf("Decoded message:\n%s\n", input);
}

void decode(char str[], int offset) {
    int index = 0;
    while (str[index] != '\0') {
        str[index] -= offset;
        index++;
    }
}