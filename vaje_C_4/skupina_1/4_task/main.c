#include <stdio.h>

void concat_string(char string1[], char string2[]);

int main() {
    char first_string[40] = "Hello ";
    char second_string[40] = "World!";
    concat_string(first_string, second_string);
    printf("Concatinated string: %s\n", first_string);
}

void concat_string(char string1[], char string2[]) {
    int offset = 0;
    while (string1[offset] != '\0') {
        offset++;
    }
    int index = 0;
    while (string2[index] != '\0') {
        string1[index + offset] = string2[index];
        index++;
    }
}