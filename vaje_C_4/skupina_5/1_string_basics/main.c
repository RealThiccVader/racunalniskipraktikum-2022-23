#include <stdio.h>

int main() {
    // String myString = "blabla";
    char my_string[50] = "My string";
    char second_string[50] = {'B', 'l', 'a', '\0'};

    for (int i = 0 ; i < 4 ; i++) {
        printf("%c", second_string[i]);
    }
    printf("\n");

    int index = 0;
    while (second_string[index] != '\0') {
        printf("%c", second_string[index]);
        index++;
    }
    printf("\n");

    printf("%s\n", second_string);
}