#include <stdio.h>

int main() {
    int key = 3;

    char message[2000];
    printf("Enter message: ");
    fgets(message, sizeof(message), stdin);

    int index = 0;
    while (message[index] != '\0') {
        message[index] -= key;
        index++;
    }
    printf("Decrypted message: %s\n", message); 
}