#include <stdio.h>

void copy_string(char from[], char to[]);

int main() {
    char origin[50] = "string i want to copy";
    char destination[50];    
    copy_string(origin, destination);
    printf("Destination string: %s\n", destination);
}

void copy_string(char from[], char to[]) {
    int index = 0;
    while (from[index] != '\0') {
        to[index] = from[index];
        index++;
    }
    to[index] = '\0';
}