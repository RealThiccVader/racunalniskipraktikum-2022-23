#include <stdio.h>

int main() {
    // String myString = "blabla";
    char first_string[20] = "My string";
    char second_string[] = {'B', 'l', 'a', '\0'};

    for (int i = 0 ; i < 10 ; i++) {
        printf("%c", first_string[i]);
    }
    printf("\n");

    int index = 0;
    while (first_string[index] != '\0') {
        printf("%c", first_string[index]);
        index++;
    }
    printf("\n");

    printf("%s\n", first_string);
}