#include <stdio.h>

int main() {
    int key = 3;

    char input[50];
    printf("Enter message: ");
    fgets(input, sizeof(input), stdin);

    int index = 0;
    while (input[index] != '\0') {
        input[index] -= key;
        index++;
    }

    printf("Decrypted message: %s\n", input);
}