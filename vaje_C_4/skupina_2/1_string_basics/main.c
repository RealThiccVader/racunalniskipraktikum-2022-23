#include <stdio.h>

int main() {
    // String myString = "blabla";
    char first_string[] = "Bla bla";
    char second_string[] = {'B', 'l', 'a', ' ', 'b', 'l', 'a', '\0'};

    for (int i = 0 ; i < 8 ; i++) {
        printf("%c", first_string[i]);
    }
    printf("\n");

    int index = 0;
    while (first_string[index] != '\0') {
        printf("%c", first_string[index]);
        index++;
    }
    printf("\n");
    printf("%s\n", first_string);
}