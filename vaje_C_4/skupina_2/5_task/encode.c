#include <stdio.h>

int main() {
    int offset = 3;
    char input[30];
    printf("Enter string to encode: ");
    fgets(input, sizeof(input), stdin);

    int index = 0;
    while (input[index] != '\0') {
        input[index] += offset;
        index++;
    }

    printf("Encrypted message: \n%s\n", input);
}