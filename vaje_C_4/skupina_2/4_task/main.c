#include <stdio.h>

void concat(char str1[], char str2[]);

int main() {
    char str1[20] = "Hello ";
    char str2[20] = "World!";
    concat(str1, str2);
    printf("%s\n", str1);
}

void concat(char str1[], char str2[]) {
    int offset = 0;
    while (str1[offset] != '\0') {
        offset++;
    }
    int index = 0;
    while (str2[index] != '\0') {
        str1[index + offset] = str2[index];
        index++;
    }
    str1[offset + index] = '\0';
}