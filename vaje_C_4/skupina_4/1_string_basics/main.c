#include <stdio.h>

int main() {
    // String bla = "blabla";

    char first_string[50] = "hello";
    char second_string[50] = {'H', 'e', 'l', 'l', 'o', '\0'};

    for (int i = 0 ; i < 6 ; i++) {
        printf("%c", second_string[i]);
    }
    printf("\n");

    int index = 0;
    while (second_string[index] != '\0') {
        printf("%c", second_string[index]);
        index++;
    }
    
    printf("\n");
    printf("%s\n", second_string);
}