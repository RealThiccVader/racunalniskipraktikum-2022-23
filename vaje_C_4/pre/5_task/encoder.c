/**
    NAL: Caesar cipher: Napiši program, ki od uporabnika sprejme string (niz besed). 
    V programu implementirajte funkciji "encode" in "decode", ki kriptirata in dekriptirata
    dan uporabnikov niz. Obe funkciji sprejmeta string (polje črk) in število, ki predstavlja
    zamik črk, vračata pa ničesar.

    IZPIS:
    What do you want me to encode: Hello, my name is Domen!
    Encoded:
    C`ggj't\h`njh`i
    Decoded:
    Hello, my name is Domen!
*/
#include <stdio.h>

void encode(char str[], int offset);

int main() {
    int offset = 3;

    char input[30];
    printf("What do you want me to encode: ");
    fgets(input, sizeof(input), stdin);

    encode(input, offset);
    printf("Encoded:\n%s\n", input);
}

void encode(char str[], int offset) {
    int index = 0;
    while (str[index] != '\0') {
        str[index] += offset;
        index++;
    }
}