/**
    NAL: Caesar cipher: Napiši program, ki od uporabnika sprejme string (niz besed). 
    V programu implementirajte funkciji "encode" in "decode", ki kriptirata in dekriptirata
    dan uporabnikov niz. Obe funkciji sprejmeta string (polje črk) in število, ki predstavlja
    zamik črk, vračata pa ničesar.

    IZPIS:
    What do you want me to encode: Hello, my name is Domen!
    Encoded:
    C`ggj't\h`njh`i
    Decoded:
    Hello, my name is Domen!
*/
#include <stdio.h>

void decode(char str[], int offset);

int main() {
    int offset = 5;

    char input[30];
    printf("What do you want me to decode: ");
    fgets(input, sizeof(input), stdin);

    decode(input, offset);
    printf("Decoded:\n%s\n", input);
}

void decode(char str[], int offset) {
    int index = 0;
    while (str[index] != '\0') {
        str[index] -= offset;
        index++;
    }
}