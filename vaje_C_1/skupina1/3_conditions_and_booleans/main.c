#include <stdio.h>
#include <stdbool.h>

int main() {
    if (1) {
        printf("Hello world!\n");
    }

    if (0) {
        printf("Not printing hello world!\n");
    }

    bool hello_again = true;
    bool are_we_sure = true;
    if (hello_again && are_we_sure) {
        printf("Hello again\n");
    }
}
