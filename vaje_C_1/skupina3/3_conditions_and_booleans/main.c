#include <stdio.h>
#include <stdbool.h>

int main() {
    if (1) {
        printf("Hello world!\n");
    }

    if (0) {
        printf("Not hello world!\n");
    }

    // must include <stdbool.h>
    bool say_hello_again = true;
    bool are_you_sure = true;
    if (say_hello_again && are_you_sure) {
        printf("Hello again!\n");
    }
}