/**
    NAL: definiraj spremenljivko "crka", ki je tipa character. Program naj
    za poljubno crko spremeni njeno velikost. V kolikor je crka mala, naj 
    jo spremeni v veliko in obratno (a -> A | A -> a)

    IZPIS:
    Crka pred spremembo: b
    Crka po spremembi: B

*/

#include <stdio.h>

int main() {
    char crka = 'a';

    printf("Crka pred spremebo: %c\n", crka);

    if (crka < 'a') {
        crka = crka + 32;
    } else {
        crka = crka - 32;
    }

    printf("Crka po spremebi: %c\n", crka);
}